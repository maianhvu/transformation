//
//  Matrix.cpp
//  Transformation
//
//  Created by Anh Vu Mai on 18/2/16.
//
//

#include <iostream>
#include <string>
#include "Matrix.h"

using namespace std;

Matrix::Matrix(int size) {
  _matrix_size = size;
  _matrix = new int[size * size];
  _temp_matrix = new int[size * size];
}

int Matrix::get(int row, int col) {
  return _matrix[row * _matrix_size + col];
}

void Matrix::set(int row, int col, int value) {
  _matrix[row * _matrix_size + col] = value;
}

void Matrix::input() {
  int i, j, value;
  for (i = 0; i < _matrix_size; i++)
    for (j = 0; j < _matrix_size; j++) {
      cin >> value;
      set(i, j, value);
    }
}

void Matrix::print() {
  int i, j;
  for (i = 0; i < _matrix_size; i++) {
    for (j = 0; j < _matrix_size; j++) {
      if (j != 0) {
        cout << " ";
      }
      cout << get(i, j);
    }
    cout << endl;
  }
}

void Matrix::rotate90() {
  for (int i = 0; i < _matrix_size; i++)
    for (int j = 0; j < _matrix_size; j++)
      _temp_matrix[i * _matrix_size + j] = get(_matrix_size - j - 1, i);
  swapPointers();
}

void Matrix::rotate180() {
  for (int i = 0; i < _matrix_size; i++)
    for (int j = 0; j < _matrix_size; j++)
      _temp_matrix[i * _matrix_size + j] = get(_matrix_size - i - 1, _matrix_size - j - 1);
  swapPointers();
}

void Matrix::rotate270() {
  for (int i = 0; i < _matrix_size; i++)
    for (int j = 0; j < _matrix_size; j++)
      _temp_matrix[i * _matrix_size + j] = get(j, _matrix_size - i - 1);
  swapPointers();
}

void Matrix::swapPointers() {
  int * swap = _matrix;
  _matrix = _temp_matrix;
  _temp_matrix = swap;
}

void Matrix::rotate(string degrees) {
  if (degrees == "90") {
      rotate90();
  } else if (degrees == "180") {
      rotate180();
  } else if (degrees == "270") {
      rotate270();
  }
}

void Matrix::reflectX() {
  for (int i = 0; i < _matrix_size; i++)
    for (int j = 0; j < _matrix_size; j++)
      _temp_matrix[i * _matrix_size + j] = get(_matrix_size - i - 1, j);
  swapPointers();
}

void Matrix::reflectY() {
  for (int i = 0; i < _matrix_size; i++)
    for (int j = 0; j < _matrix_size; j++)
      _temp_matrix[i * _matrix_size + j] = get(i, _matrix_size - j -1);
  swapPointers();
}

void Matrix::perform(string op, string param) {
  if (op == "Rotate") {
    rotate(param);
  } else if (op == "Reflect") {
    if (param == "x") {
      reflectX();
    } else if (param == "y") {
      reflectY();
    }
  }
}
