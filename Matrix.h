//
//  Matrix.h
//  Transformation
//
//  Created by Anh Vu Mai on 18/2/16.
//
//

#ifndef MATRIX_H
#define MATRIX_H

#include <string>

using namespace std;

class Matrix {
  private:
    int _matrix_size;

    int *_matrix;
    int *_temp_matrix;

    int get(int, int);
    void set(int, int, int);

    void swapPointers();

    void rotate90();
    void rotate180();
    void rotate270();
    void rotate(string);

    void reflectX();
    void reflectY();

  public:
    Matrix(int);

    void input();
    void print();

    void perform(string, string);
};


#endif /* Matrix_h */
