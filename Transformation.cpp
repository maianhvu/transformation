//
//  Transformation.cpp
//  Transformation
//
//  Created by Anh Vu Mai on 18/2/16.
//
//

#include <iostream>
#include <string>
#include "Matrix.h"

using namespace std;

int main() {
  int N, K;
  string op, param;
  Matrix *matrix;

  // Input matrix size
  cin >> N;
  matrix = new Matrix(N);

  // Read in matrix
  matrix->input();

  // Input number of operations
  cin >> K;
  for (int i = 0; i < K; i++) {
    cin >> op >> param;
    matrix->perform(op, param);
  }

  matrix->print();

  return 0;
}
